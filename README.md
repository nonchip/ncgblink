# ncGBLink

**THIS DOES NOT WORK YET**

## Hardware

### You'll need:

* [Raspi Pico](https://www.raspberrypi.org/products/raspberry-pi-pico/)
* Easy route:
  * [Stacksmashing's adapter board](https://gumroad.com/l/gb-link)
* DIY route:
  * [4 "fet+resistor" level shifters](https://learn.sparkfun.com/tutorials/bi-directional-logic-level-converter-hookup-guide/all)
    * those can *technically* be replaced by just 4 100Ohm inline resistors **if** you only **ever** use it with a GBA, on your own risk.
  * Link port breakout or I guess cut an aftermarket cable to get to the wires

### Assembly:

* Easy route:
  * just solder the pico to the board (it's called the easy route for a reason)
* DIY route:
  * connect `GND` (e.g. pin 3 of pico) to `GND`
  * connect the following via level shifters (LV side is pico, HV side is link port):
    * `GP0` to `SC`
    * `GP1` to `SI`
    * `GP2` to `SO`
    * `GP3` to `SD`
    * `3V3OUT` (pin 36) to level-shifter's `LVCC`
    * depending on target hardware, to level-shifter's `HVCC`:
      * for GB(C): `VBUS` (pin 40)
      * for GBA: `3V3OUT` (pin 36)

## Firmware

```
cd firmware
mkdir build
cd build
cmake ..
make
```
then flash `firmware/build/firmware.uf2` to your pico
