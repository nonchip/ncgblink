#include <arpa/inet.h>
#include <cerrno>
#include <chrono>
#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <iostream>
#include <string>
#include <sys/ioctl.h>
#include <termios.h>
#include <thread>
#include <unistd.h>
using namespace std;

int main(int argc, char *argv[]) {
	uint16_t data[4] = {0};
	string ser_path  = "/dev/ttyACM0";
	int ser_fd       = open(ser_path.c_str(), O_RDWR);

	if (ser_fd < 0) {
		cerr << "Failed to open: " << ser_path << endl;
		return 1;
	}

	{
		struct termios tty;
		if (tcgetattr(ser_fd, &tty) != 0) printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
		tty.c_cflag &= ~PARENB;                 // no parity
		tty.c_cflag &= ~CSTOPB;                 // 1 stop
		tty.c_cflag |= CS8;                     // 8 bits
		tty.c_cflag &= ~CRTSCTS;                // no HW flow control
		tty.c_cflag |= CREAD | CLOCAL;          // read, no carrier detect
		tty.c_lflag &= ~ICANON;                 // disable canonical (line-based) mode
		tty.c_lflag &= ~ECHO;                   // Disable echo
		tty.c_lflag &= ~ECHOE;                  // Disable erasure
		tty.c_lflag &= ~ECHONL;                 // Disable new-line echo
		tty.c_lflag &= ~ISIG;                   // Disable interpretation of INTR, QUIT and SUSP
		tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off SW flow ctrl
		tty.c_iflag
		 &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL); // Disable any special handling of received bytes
		tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
		tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
		tty.c_cc[VTIME] = 10;  // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
		tty.c_cc[VMIN]  = sizeof data;
		cfsetspeed(&tty, B115200);
		if (tcsetattr(ser_fd, TCSANOW, &tty) != 0) printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
	}

	int stage       = 0;
	int counter     = 0;
	uint8_t clients = 0;
	uint16_t out    = 0;
	while (1) {
		int bytes;
		ioctl(ser_fd, FIONREAD, &bytes);
		if (bytes >= (sizeof data)) {
			int n = read(ser_fd, reinterpret_cast<char *>(&data), sizeof data);
			if (n < 0)
				printf("Error %i from read: %s\n", errno, strerror(errno));
			else if (n < (sizeof data)) {
				printf("Error: read only %d bytes\n", n);
				data[0] = 0xFFFF;
			}
		} else {
			data[0] = 0xFFFF;
		}
		for (int i = 0; i < 4; i++) data[i] = ntohs(data[i]);
		printf("IN %04X %04X %04X %04X %d %d %d \t", data[0], data[1], data[2], data[3], stage, counter, clients);
		if (data[0] != 0xffff) switch (stage) {
				case 0: // wait for ready
					counter = 0;
					if (data[1] == 0x0000) stage++;
					out = 0x6200;
					break;
				case 1: // sync 15 times
					if ((data[1] & 0xFFF0) == 0x7200) {
						clients = data[1] & 0xF;
						if (++counter == 15) stage++;
						out = 0x6100 | clients;
						break;
					} else {
						stage = 0;
					}
					out = 0x6200;
					break;
				case 2: // handshake
					counter = 0;
					if (data[1] == (0x7200 | clients)) stage++;
					out = 0x6100 | clients;
					break;
				case 3: // header data
					if (++counter == 0x60) stage++;
					out = 0x0000;
					break;
				default: stage = 0; break;
			}
		data[0] = out;
		printf("OUT %04X %d %d %d\n", data[0], stage, counter, clients);
		data[0] = htons(data[0]);
		write(ser_fd, reinterpret_cast<char *>(&data[0]), sizeof data[0]);
		//	std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}