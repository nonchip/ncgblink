#include "forth.hpp"

#include <ios>
#include <iostream>
#include <ostream>
#include <variant>

using namespace ncxxforth;
using namespace std;
using namespace std::literals;

namespace {
	static Word stack_one{{
	                       [](Forth &f) {
		                       if (f.stack.size() < 1) {
			                       cerr << "Stack empty!" << endl;
			                       f.breaks = 2;
		                       }
	                       },
	                      },
	                      false};

	static Word stack_drop{{
	                        &stack_one,
	                        [](Forth &f) { f.stack.pop(); },
	                       },
	                       false};

	static Word stack_num{{
	                       &stack_one,
	                       [](Forth &f) {
		                       if (!holds_alternative<tcell::Number>(f.stack.top())) {
			                       cerr << "Expected Number: " << to_string(f.stack.top()) << endl;
			                       f.breaks = 2;
		                       }
	                       },
	                      },
	                      false};

	static Word stack_min{{
	                       &stack_one,
	                       &stack_num,
	                       [](Forth &f) {
		                       auto i = static_cast<size_t>(get<tcell::Number>(f.stack.top()));
		                       f.stack.pop();
		                       if (f.stack.size() < i) {
			                       cerr << "Expected Stack >= " << i << endl;
			                       f.breaks = 2;
		                       }
	                       },
	                      },
	                      false};

	static Word pop_to_local{{
	                          Cell((tcell::Number)2),
	                          &stack_min,
	                          &stack_num,
	                          [](Forth &f) {
		                          auto i = static_cast<size_t>(get<tcell::Number>(f.stack.top()));
		                          f.stack.pop();
		                          f.exec_parent->content[i] = f.stack.top();
		                          f.stack.pop();
	                          },
	                         },
	                         false};

	static Word push_from_local{{
	                             &stack_one,
	                             &stack_num,
	                             [](Forth &f) {
		                             auto i = static_cast<size_t>(get<tcell::Number>(f.stack.top()));
		                             f.stack.pop();
		                             f.stack.push(f.exec_parent->content[i]);
	                             },
	                            },
	                            false};
	static Word skip_n{{
	                    &stack_one,
	                    &stack_num,

	                    [](Forth &f) {
		                    f.breaks = 1;
		                    f.jumps  = get<tcell::Number>(f.stack.top()) + 1;
		                    f.stack.pop();
	                    },
	                   },
	                   true};

	template <typename Op> static Word op2{
	 {
	  Cell((tcell::Number)2),
	  &skip_n,
	  Cell(), // placeholder a
	  Cell(), // placeholder b

	  Cell((tcell::Number)2),
	  &stack_min,

	  &stack_num,
	  Cell((tcell::Number)2), // a
	  &pop_to_local,

	  &stack_num,
	  Cell((tcell::Number)3), // b
	  &pop_to_local,

	  [](Forth &f) {
		  Op o;
		  f.stack.push(o(get<tcell::Number>(f.executing->content[2]), get<tcell::Number>(f.executing->content[3])));
	  },
	 },
	 false};

	template <typename Op> static Word op1{{
	                                        Cell((tcell::Number)1),
	                                        &skip_n,
	                                        Cell(), // placeholder a

	                                        &stack_one,

	                                        &stack_num,
	                                        Cell((tcell::Number)2), // a
	                                        &pop_to_local,

	                                        [](Forth &f) {
		                                        Op o;
		                                        f.stack.push(o(get<tcell::Number>(f.executing->content[2])));
	                                        },
	                                       },
	                                       false};

	static Word semi{{
	                  [](Forth &f) { f.breaks = 2; },
	                 },
	                 true};

	static Word colon{{
	                   [](Forth &f) {
		                   tcell::String name = f.read_word();
		                   Word *word         = new Word(f.compile_inplace());
		                   f.dict.insert_or_assign(name, word);
	                   },
	                  },
	                  false};

	static Word period{{
	                    &stack_one,
	                    [](Forth &f) {
		                    cout << to_string(f.stack.top(), &f) << consts::svENDL;
		                    // formatting borked
	                    },
	                    &stack_drop,
	                   },
	                   false};

	/*
	static Word swap{{
	                  Cell((tcell::Number)2),
	                  &skip_n,
	                  Cell(), // placeholder a
	                  Cell(), // placeholder b

	                  Cell((tcell::Number)2),
	                  &stack_min,

	                  &stack_num,
	                  Cell((tcell::Number)2), // a
	                  &pop_to_local,

	                  &stack_num,
	                  Cell((tcell::Number)3), // b
	                  &pop_to_local,

	                  Cell((tcell::Number)2), // a
	                  &push_from_local,

	                  Cell((tcell::Number)3), // b
	                  &push_from_local,
	                 },
	                 false};
	 */

	static Word paren{{
	                   [](Forth &f) { f.read_word(")"); },
	                  },
	                  true};

	static Cell GLOBA, GLOBB, GLOBC;
	static Word toA{{&stack_one, [](Forth &f) { GLOBA = f.stack.top(); }, &stack_drop}, false};
	static Word toB{{&stack_one, [](Forth &f) { GLOBB = f.stack.top(); }, &stack_drop}, false};
	static Word toC{{&stack_one, [](Forth &f) { GLOBC = f.stack.top(); }, &stack_drop}, false};
	static Word fromA{{[](Forth &f) { f.stack.push(GLOBA); }}, false};
	static Word fromB{{[](Forth &f) { f.stack.push(GLOBB); }}, false};
	static Word fromC{{[](Forth &f) { f.stack.push(GLOBC); }}, false};
}

// clang-format off
Forth FORTH{{
	{"+", &op2< plus<tcell::Number> >},
	{"-", &op2< minus<tcell::Number> >},
	{"*", &op2< multiplies<tcell::Number> >},
	{"/", &op2< divides<tcell::Number> >},
	{"%", &op2< modulus<tcell::Number> >},
	{"0-", &op1< negate<tcell::Number> >},
	{":", &colon},
	{";", &semi},
	{".", &period},
	{"(", &paren},
	{"A!",&toA},{"A@",&fromA},
	{"B!",&toB},{"B@",&fromB},
	{"C!",&toC},{"C@",&fromC},
},R"(

	: DROP ( x -- ) A! ;
	: SWAP ( x y -- y x ) A! B! A@ B@ ;

)"};
// clang-format on
