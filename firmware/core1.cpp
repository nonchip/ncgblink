#include "core1.h"

#include "hardware/sync.h"
#include "pico/multicore.h"
#include "pico/util/queue.h"

namespace {
	static queue_t queue0; // READ by core0 (main)
	static queue_t queue1; // READ by core1
	static bool init_done = false;
};

namespace core1 {

	queue_packet &get() {
		static queue_packet pack;
		queue_remove_blocking(get_core_num() ? &queue1 : &queue0, &pack);
		return pack;
	}

	void put(queue_packet &pack) { queue_add_blocking(get_core_num() ? &queue0 : &queue1, &pack); }
	bool have() { return !queue_is_empty(get_core_num() ? &queue1 : &queue0); }
	queue_packet &peek() {
		static queue_packet pack;
		queue_peek_blocking(get_core_num() ? &queue1 : &queue0, &pack);
		return pack;
	}

	void dispatch(void (*entry)(void)) {
		if (get_core_num()) panic("can't dispatch core1 from core1");
		multicore_reset_core1();
		if (queue0.data) queue_free(&queue0);
		if (queue1.data) queue_free(&queue1);
		queue_init(&queue0, sizeof(queue_packet), 8);
		queue_init(&queue1, sizeof(queue_packet), 8);
		multicore_launch_core1(entry);
	}
};
