#pragma once

/*
 NORMAL cable:     MULTI cable:
 VCC---VCC         VCC------VCC-----VCC-----VCC
 SO-\/-SO          SO---\   SO--\   SO--\   SO
 SI-/\-SI          SI--\ \--SI   \--SI   \--SI
 SD    SD          SD--+----SD------SD------SD
 SC----SC          SC--+----SC------SC------SC
 GND---GND         GND-*----GND-----GND-----GND

  _____/\_____ cable plug
 / VCC SI SC  \
 |__SO_SD_GND_|

  adapter debug pins: VCC SO SI SD SC GND

 pico pinout:
 SC  GP0  [usb]   VBUS
 SI  GP1 [led:25] VSYS
     GND          GND
 SO  GP2          3v3en
 SD  GP3          3V3out
     GP4                ADVREF
     GP5          GP28  ADC2
     GND          GND   AGND
     GP6          GP27  ADC1
     GP7          GP26  ADC0
     GP8          RUN
     GP9          GP22
     GND          GND
     GP10         GP21
     GP11         GP20
     GP12         GP19
     GP13         GP18
     GND          GND
     GP14         GP17
     GP15 [debug] GP16
*/

enum pins {
	onboard_led = PICO_DEFAULT_LED_PIN,

	link_si = 1,
	link_sc = 0,
	link_so = 2,
	link_sd = 3,

	link_si_bit = 1 << link_si,
	link_sc_bit = 1 << link_sc,
	link_so_bit = 1 << link_so,
	link_sd_bit = 1 << link_sd,

	link_bits = link_si_bit | link_sc_bit | link_so_bit | link_sd_bit,
};
