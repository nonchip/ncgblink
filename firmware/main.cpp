#include "core1.h"
#include "forth.hpp"
#include "pico/binary_info.h"
#include "pico/stdlib.h"
#include "pico/time.h"
#include "pins.h"

#include <cinttypes>
#include <iostream>
#include <stdio.h>
#include <string>
#include <tusb.h>

static void waitCDC() { // wait for CDC while blinking
	while (true) {
		if (stdio_usb_connected()) return;
		gpio_put(pins::onboard_led, 0);
		sleep_ms(100);
		gpio_put(pins::onboard_led, 1);
		sleep_ms(100);
	}
}

int main() {
	bi_decl(bi_program_description("NCGBLINK"));
	bi_decl(bi_1pin_with_name(pins::onboard_led, "On-board LED"));
	bi_decl(bi_1pin_with_name(pins::link_sc, "Link SC"));
	bi_decl(bi_1pin_with_name(pins::link_sd, "Link SD"));
	bi_decl(bi_1pin_with_name(pins::link_si, "Link SI"));
	bi_decl(bi_1pin_with_name(pins::link_so, "Link SO"));

	set_sys_clock_khz(250000, true);

	gpio_init(pins::onboard_led);
	gpio_set_dir(pins::onboard_led, GPIO_OUT);

	core1::dispatch(core1::link::multi<core1::link::m_speed::b115200>);
	//	core1::queue_packet p{.timestamp = time_us_64(), .command = core1::queue_packet::command::n8, .data8 = 0xD2};
	//	core1::put(p);

	stdio_usb_init();

	uint64_t last_recv = 0;
	std::string line;
	while (true) {
		waitCDC();
		//	std::getline(std::cin, line);
		//	FORTH.eval(line);
		if (tud_cdc_available() >= 2) {
			core1::queue_packet p{.timestamp = time_us_64(), .command = core1::queue_packet::command::m, .data16 = {0xFFFF}};
			std::cin.read(reinterpret_cast<char *>(&p.data16[0]), sizeof(p.data16[0]));
			p.data16[0] = tu_ntohs(p.data16[0]);
			core1::put(p);
		}
		if (core1::have()) {
			core1::queue_packet p = core1::get();
			last_recv             = p.timestamp;
			for (int i = 0; i < 4; i++) p.data16[i] = tu_htons(p.data16[i]);
			std::cout.write(reinterpret_cast<char *>(&p.data16), sizeof(p.data16));
		}
		/*if ( tud_cdc_available() >= 4) {
		 core1::queue_packet p{.timestamp = time_us_64(), .command = core1::queue_packet::command::n32, .data32 = 0};
		 std::cin.read(reinterpret_cast<char *>(&p.data32), sizeof(p.data32));
		 p.data32 = tu_ntohl(p.data32);
		 p.data32++;
		 p.data32 = tu_htonl(p.data32);
		 std::cout.write(reinterpret_cast<char *>(&p.data32), sizeof(p.data32));
		}*/
	}

	__builtin_unreachable();
	return 0;
}
