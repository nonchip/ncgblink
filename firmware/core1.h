#pragma once

#include <cstdint>

namespace core1 {

	struct queue_packet {
			uint64_t timestamp;
			enum command {
				n8,
				n32,
				m,
			} command;
			union {
					uint8_t data8;
					uint16_t data16[4];
					uint32_t data32;
			};
	};

	queue_packet &get();
	queue_packet &peek();
	void put(queue_packet &);
	bool have();
	void dispatch(void (*)(void));

	namespace link {
		enum n_bits { b8, b32 };
		enum n_speed { slow, fast };
		template <n_bits bits = b8> void normal_ext();
		template <n_bits bits = b8, n_speed speed = slow> void normal_int();

		enum m_speed { b9600, b38400, b57600, b115200 };
		template <m_speed speed = b115200> void multi();
	}
};
