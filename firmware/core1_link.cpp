#include "core1.h"
#include "hardware/gpio.h"
#include "pico/time.h"
#include "pins.h"

#include <type_traits>

namespace {
	static inline uint8_t wait_sc(bool high, uint64_t timeout = 0) {
		uint32_t all_gpios;
		do {
			all_gpios = gpio_get_all();
			if (timeout > 0 && timeout < time_us_64()) { return 0; }
		} while (high == !(all_gpios & pins::link_sc_bit));
		return all_gpios & pins::link_bits;
	}
};

namespace core1::link {

	template void normal_ext<n_bits::b8>();
	template void normal_ext<n_bits::b32>();

	template <n_bits bits> void normal_ext() {
		constexpr bool eight = (bits == n_bits::b8);
		gpio_init_mask(pins::link_bits);
		gpio_set_dir_masked(pins::link_bits, pins::link_so_bit);
		std::conditional_t<eight, uint8_t, uint32_t> tx = 0;
	start_over:
		while (true) {
			if (have()) {
				auto pt = get();
				if constexpr (eight) {
					if (pt.command == queue_packet::command::n8) tx = pt.data8;
				} else {
					if (pt.command == queue_packet::command::n32) tx = pt.data32;
				}
			}
			wait_sc(false);
			queue_packet pr;
			if constexpr (eight) {
				pr = {.timestamp = time_us_64(), .command = queue_packet::command::n8, .data8 = 0};
			} else {
				pr = {.timestamp = time_us_64(), .command = queue_packet::command::n32, .data32 = 0};
			}
			for (int i = (eight ? 7 : 31); i >= 0; i--) {
				gpio_put(link_so, !!(tx & (1 << i)));
				auto link_gpios = wait_sc(true, pr.timestamp + (eight ? 35 /* 8/256KHz = 31.25us */ : 130 /* 32/256KHz = 125us */));
				if (link_gpios == 0) goto start_over; // timeout
				if constexpr (eight) {
					pr.data8 |= !!(link_gpios & pins::link_si_bit) << i;
				} else {
					pr.data32 |= !!(link_gpios & pins::link_si_bit) << i;
				}
				if (i) wait_sc(false); // don't wait after the last bit
			}
			put(pr);
		}

		__builtin_unreachable();
	}

	template void normal_int<n_bits::b8, n_speed::slow>();
	template void normal_int<n_bits::b8, n_speed::fast>();
	template void normal_int<n_bits::b32, n_speed::slow>();
	template void normal_int<n_bits::b32, n_speed::fast>();

	template <n_bits bits, n_speed speed> void normal_int() {
		constexpr bool eight = (bits == n_bits::b8);
		gpio_init_mask(pins::link_bits);
		gpio_set_dir_masked(pins::link_bits, pins::link_so_bit | pins::link_sc_bit);
		while (true) {
			auto pt = get();
			if ((eight && pt.command == queue_packet::command::n8)
			    || (bits == n_bits::b32 && pt.command == queue_packet::command::n32)) {
				gpio_put(link_sc, false);
				queue_packet pr;
				if constexpr (eight) {
					pr = {.timestamp = time_us_64(), .command = queue_packet::command::n8, .data8 = 0};
				} else {
					pr = {.timestamp = time_us_64(), .command = queue_packet::command::n32, .data32 = 0};
				}
				for (int i = (eight ? 7 : 31); i >= 0; i--) {
					gpio_put(link_so, !!((eight ? pt.data8 : pt.data32) & (1 << i)));
					if constexpr (speed == n_speed::slow) busy_wait_us(2);
					gpio_put(link_sc, true);
					if constexpr (eight) {
						pr.data8 |= !!gpio_get(link_si) << i;
					} else {
						pr.data32 |= !!gpio_get(link_si) << i;
					}
					if constexpr (speed == n_speed::slow) busy_wait_us(2);
					if (i) gpio_put(link_sc, false);
				}
				put(pr);
			}
		}

		__builtin_unreachable();
	}

	//	template void multi<m_speed::b9600>();
	//	template void multi<m_speed::b38400>();
	//	template void multi<m_speed::b57600>();
	template void multi<m_speed::b115200>();

	template <m_speed speed> void multi() {
		static_assert(speed == m_speed::b115200, "only 115.2kHz supported!");
		gpio_init_mask(pins::link_bits);
		uint16_t tx;
		while (true) {
		start_over:
			if (have()) {
				auto pt = get();
				if (pt.command == queue_packet::command::m) tx = pt.data16[0];
			}
			queue_packet pr{.timestamp = time_us_64(), .command = queue_packet::command::m, .data16 = {0}};
			pr.data16[0] = tx;
			pr.data16[1] = 0xFFFF;
			pr.data16[2] = 0xFFFF;
			pr.data16[3] = 0xFFFF;
			gpio_set_dir_masked(pins::link_bits, link_so_bit);
			gpio_put(link_so, true);

			// wait for SC=1 (no transfer) and SD=1 (all gbas ready)
			for (int i = 0; !(gpio_get(link_sc) && gpio_get(link_sd)); i++) {
				busy_wait_us(1);
				if (i > 1000) goto start_over;
			}

			bool had_turn = false;
			for (int client = 0; client < 4; client++) {

				if (!had_turn && !gpio_get(link_si)) { // our turn
					// all output
					gpio_set_dir_masked(pins::link_bits, pins::link_so_bit | pins::link_sc_bit | pins::link_sd_bit);
					// set SC=0 to start transfer if we're master
					if (client == 0) gpio_put(link_sc, false);
					// send 0[start] [16bit msb data] 1[stop]
					for (int i = 17; i >= 0; i--) {
						switch (i) {
							case 17: gpio_put(link_sd, false); break;
							case 0: gpio_put(link_sd, true); break;
							default: gpio_put(link_sd, !!(tx & (1 << (i - 1))));
						}
						// 8.6ish for 115200
						if (i % 3 == 0)
							busy_wait_us(8);
						else
							busy_wait_us(9);
					}
					// forward SO=0
					gpio_put(link_so, false);
					had_turn = true;

				} else { // someone else's turn
					// SD input (but keep SC "sticky")
					gpio_set_dir_masked(pins::link_bits, pins::link_so_bit | pins::link_sc_bit);
					// wait for 0[start]
					for (int t = 0; gpio_get(link_sd); t++) {
						busy_wait_us(1);
						if (t > 1000) {
							gpio_put(link_sc, true);
							goto start_over;
						}
					}
					pr.data16[client] = 0;
					// receive ignored[start] [16bit msb data] ignored[stop]
					for (int i = 17; i >= 0; i--) {
						switch (i) {
							case 17: busy_wait_us(4); // get into middle
							case 0: break;
							default: pr.data16[client] |= !!gpio_get(link_sd) << (i - 1);
						}
						if (i < 17) {
							// 8.6ish for 115200
							if (i % 3 == 0)
								busy_wait_us(8);
							else
								busy_wait_us(9);
						}
					}
				}
			}
			// "stop" transfer (opendrain so not really harmful to do if we're not the master)
			gpio_put(link_sc, true);
			put(pr);
			tx = 0xFFFF;
		}

		__builtin_unreachable();
	}
};
